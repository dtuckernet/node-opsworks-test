var express = require('express');
var metadata = require('node-ec2-metadata');
var Q = require('q');
var app = express();
var fs = require('fs');
var validate = require('validate.js');
var winston = require('winston');
var _ = require('lodash');
var Redis = require('ioredis');

var passport = require('passport');
var session = require('express-session');
var Strategy = require('passport-twitter').Strategy;
var RedisStore = require('connect-redis')(session);

var OPSWORKS_CONFIG_FILENAME = 'config-opsworks.json';
var LOCAL_CONFIG_FILENAME = 'config/local-config.json';
var CONFIG_SCHEME_FILE = 'config/config-schema.json';
var EC_CONFIG_SCHEME_FILE = 'config/elasticache-node-schema.json';

winston.add(winston.transports.File, { filename: 'log/app.log' });

// CONFIG LOADING ------------------------------------------------

var config = {};
var instance_id = "";
var ami_id = "";
var hostname = "";
var public_hostname = "";
var public_ipv4 = "";
var server = {};
var redis_client = {};

var isOpsworksConfigPresent = function() {
    var stats = {};
    try {
        stats = fs.statSync(OPSWORKS_CONFIG_FILENAME);
    } catch(err) {
        winston.info('Opsworks Config File is not Present, assuming local config.');
        return false;
    }
    winston.info('Opsworks Config File is present, assuming opsworks config.');
    return true;
}();

var isConfigValid = function() {
    var filename = (isOpsworksConfigPresent) ? OPSWORKS_CONFIG_FILENAME : LOCAL_CONFIG_FILENAME;
    var obj = JSON.parse(fs.readFileSync(filename, 'utf8'));
    var validatorSchema = JSON.parse(fs.readFileSync(CONFIG_SCHEME_FILE, 'utf8'));
    var result = validate(obj, validatorSchema);
    if(result) {
        winston.info('App Config validation error.', { errors: result, config: obj });
        return false;
    }
    config = obj;
    return true;
}();

if(!isConfigValid) {
    winston.error('Config Invalid.... App Aborting.');
    process.exit(1);
}

// REDIS CONFIGURATION -------------------------------------------

var reconnectOnErrorHandler = function(err) {
    var targetError = 'READONLY';
    if (err.message.slice(0, targetError.length) === targetError) {
      return 2;
    }
};

redis_client = new Redis({
    port: config.elasticache.port,
    host: config.elasticache.hostname,
    family: 4,
    db: 0,
    reconnectOnError: reconnectOnErrorHandler
});


// PASSPORT CONFIGURATION ----------------------------------------

passport.use(new Strategy({
    consumerKey: 'bRSPwWdJp7TMRCnU6ILhgyy7g',
    consumerSecret: 'OS8fiiaBvmWokFOKuoouCTsIUZZfKkO53oVs9ItlkoNAZHLC9R',
    callbackURL: 'http://' + config.domainName + ':' + config.port + '/login/twitter/return'
  },
  function(token, tokenSecret, profile, cb) {
    return cb(null, profile);
  }
));

passport.serializeUser(function(user, cb) {
  cb(null, user);
});

passport.deserializeUser(function(obj, cb) {
  cb(null, obj);
});

var setupExpress = function() {
    app.set('views', __dirname + '/views');
    app.set('view engine', 'ejs');
    app.use(require('morgan')('combined'));
    app.use(require('cookie-parser')());
    app.use(require('body-parser').urlencoded({ extended: true }));
    app.use(session({ store: new RedisStore({
      client: redis_client
    }), resave: false, saveUninitialized: false, secret: 'hey you' }));
    app.use(passport.initialize());
    app.use(passport.session());

    // Define routes.
    app.get('/',
      function(req, res) {
        res.render('home', { user: req.user, ec2Metadata: getEC2Metadata() });
      });

    app.get('/login',
      function(req, res){
        res.render('login', { ec2Metadata: getEC2Metadata() });
      });

    app.get('/login/twitter',
      passport.authenticate('twitter'));

    app.get('/login/twitter/return',
      passport.authenticate('twitter', { failureRedirect: '/login' }),
      function(req, res) {
        res.redirect('/');
      });

    app.get('/profile',
      require('connect-ensure-login').ensureLoggedIn(),
      function(req, res){
        res.render('profile', { user: req.user, ec2Metadata: getEC2Metadata() });
      });

    server = app.listen(config.port, function () {
      var host = server.address().address;
      var port = server.address().port;
      winston.info('Example app listening at http://' + host + ':' + port);
    });
};

var getEC2Metadata = function() {
    if(isOpsworksConfigPresent) {
        return {
            instanceID: instance_id,
            hostname: hostname,
            publicHostname: public_hostname
        };
    } else {
        return {
            instanceID: 'LOCAL_DEV',
            hostname: config.domainName,
            publicHostname: config.domainName
        };
    }
};

if(isOpsworksConfigPresent) {
    Q.all([
        metadata.getMetadataForInstance('instance-id'),
        metadata.getMetadataForInstance('ami-id'),
        metadata.getMetadataForInstance('hostname'),
        metadata.getMetadataForInstance('public-hostname'),
        metadata.getMetadataForInstance('public-ipv4')
    ])
    .spread(function(instanceID, amiID, aHostname, publicHostname, publicIPv4) {
        instance_id = instanceID;
        ami_id = amiID;
        hostname = aHostname;
        public_hostname = publicHostname;
        public_ipv4 = publicIPv4;
    })
    .then(function() {
        setupExpress();
    })
    .fail(function(error) {
        console.log("Error: " + error);
    });
} else {
    setupExpress();
}
